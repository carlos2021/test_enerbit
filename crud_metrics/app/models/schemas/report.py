from redis_om import HashModel
from app.core.config import redis_db


class Task(HashModel):
    id_device: str
    metrics: str
    timestamp: str

    class Meta:
        database: redis_db
