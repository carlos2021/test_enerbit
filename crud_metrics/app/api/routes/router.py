import logging

from fastapi import APIRouter

from app.api.routes import report, analyzer

api_router = APIRouter()
api_router.include_router(report.router, tags=["Report"])
api_router.include_router(analyzer.router, tags=["Analizer"])
