from fastapi import APIRouter
from app.models.schemas.report import Task
from app.utilites.format_info import format

router = APIRouter()


@router.post("/report")
async def create(task: Task):
    return task.save()


@router.get("/reports")
async def all():
    return [format(pk) for pk in Task.all_pks()]


@router.put("/report/{pk}")
async def update(pk: str, task: Task):
    _task = Task.get(pk)
    _task.id_device = task.id_device
    _task.metrics = task.metrics
    _task.timestamp = task.timestamp

    return _task.save()


@router.delete("/report/{pk}")
async def delete(pk: str):
    _task = Task.get(pk)
    return _task.delete()
