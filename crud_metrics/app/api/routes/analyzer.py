from fastapi import APIRouter
from app.models.schemas.report import Task
from app.utilites.format_info import format
from app.utilites.generate_csv import csv_from_reports
from app.utilites.generate_image import image_from_reports

router = APIRouter()


@router.get("/generate-csv")
async def generate_csv():
    list_info = [format(pk) for pk in Task.all_pks()]
    return csv_from_reports(list_info)


@router.get("/generate-graph")
async def generate_graph():
    list_info = [format(pk) for pk in Task.all_pks()]
    return image_from_reports(list_info)
