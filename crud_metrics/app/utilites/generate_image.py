import pandas as pd
import io
from fastapi.responses import StreamingResponse
import matplotlib
import matplotlib.pyplot as plt


def format_data_whithout_kwh(data):
    data_whitouth_kwh = []
    for element in data:
        json_whitouth_kwh = element.copy()
        json_whitouth_kwh["metrics"] = json_whitouth_kwh["metrics"].replace(" kwh", "")
        data_whitouth_kwh.append(json_whitouth_kwh)
    return data_whitouth_kwh


def image_from_reports(list_data):
    data_format = format_data_whithout_kwh(list_data)
    df = pd.DataFrame.from_records(data_format)
    stream = io.BytesIO()
    my_plot = df.plot("timestamp", "metrics", kind="scatter")
    plt.savefig(stream, format="png", bbox_inches="tight")
    stream.seek(0)  # important here!
    response = StreamingResponse(
        stream,
        media_type="image/png",
        headers={"Content-Disposition": 'inline; filename="report.png"'},
    )
    return response
