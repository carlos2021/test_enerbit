from app.models.schemas.report import Task


def format(pk: str):
    task = Task.get(pk)
    return {
        "id_device": task.id_device,
        "metrics": task.metrics,
        "timestamp": task.timestamp,
    }
