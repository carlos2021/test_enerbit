import pandas as pd
import io
from fastapi.responses import StreamingResponse


def csv_from_reports(list_data):
    df = pd.DataFrame.from_records(list_data)
    stream = io.StringIO()
    df.to_csv(stream, index=False, header=True)
    response = StreamingResponse(iter([stream.getvalue()]), media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=reports.csv"
    return response
