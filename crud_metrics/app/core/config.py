from redis_om import get_redis_connection

import os
from dotenv import load_dotenv

load_dotenv()

redis_db = get_redis_connection(host=os.getenv("DB_HOST"), port="6379")
