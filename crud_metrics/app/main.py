#!/usr/bin/env python3
""" Main function """

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.api.routes.router import api_router


def get_application() -> FastAPI:
    application = FastAPI()
    application.add_middleware(CORSMiddleware, allow_origins=["*"])
    application.include_router(api_router)

    return application


app = get_application()
