# Prueba tecnica para enerbit 

este es un microservicio que permite realizar operaciones crud y dos operaciones adicionales para analizar los datos, esta configurado para correr por.


## Detalle

microservicio en fastapi que contiene un CRUD el
cual permita administrar los datos enviados por el medidor.

# Variables de entorno

Solo debes copiar y pegar el archivo .envExample, en el caso de que quieras correrlo con docker pues deja redis para que tome el host del contenedor que levanta el docker, en caso de que quieras correrlo con entorno virtual pues localhost, y pues si vas a conectarte a otro redis y no el que genera este proyecto pues le pones el host

# con docker

## Docker

El proyecto esta fuentemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción. 


## Ponerlo a correr
    $ sudo docker-compose -f local.yml build
    $ sudo docker-compose -f local.yml up




## Despliegue a producción
    - Le pone las credenciales en el .env de la bd
    $ sudo docker-compose -f production.yml build
    $ sudo docker-compose -f production.yml up
    


# con entorno
1. Crear entorno virtual:

```bash
python3  -m venv venv
```

2. Activar el entorno virual:

```bash
python -m venv/Script/activate
```

3. Instalar las dependencias necesarias:

```bash
python -m pip install -r requirements.txt -r requirements-dev.txt
```


4. Correr la aplicación y levantar servidor para los endpoints:

```bash
python -m uvicorn app.main:app --host 0.0.0.0 --port 8002
```