This is the image that contains redis and basically to work locally, if you don't want to use it you can connect to your own, it's your choice, if you choose it, you can run it with the following command

```
sudo docker-compose -f redis/redis-docker-compose.yml up -d
```