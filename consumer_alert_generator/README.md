# Prueba tecnica para enerbit 

este es un microservicio que realiza el rol de Consumer y genera alertas.


## Detalle

un microservicio conectado al mismo stream de eventos pero
que reaccione de una forma diferente, en este caso se deberá simular una alerta o notificación en caso tal de que la medida reportada supere
un umbral de 50.

Como queda la simulación? 
R/ se genera un archivo txt con el reporte de alertas y se emite un sonido


# Variables de entorno

Solo debes copiar y pegar el archivo .envExample con el nombre .env, en el caso de que quieras correrlo con docker pues dar la direccion de redis para que tome el host del contenedor que levanta el docker, en caso de que quieras correrlo con entorno virtual pues localhost, y pues si vas a conectarte a otro redis y no el que genera este proyecto pues le pones el host

# con docker

## Docker

El proyecto esta fuentemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción. 


## Ponerlo a correr
    $ sudo docker-compose -f local.yml build
    $ sudo docker-compose -f local.yml up




## Despliegue a producción
    $ sudo docker-compose -f production.yml build
    $ sudo docker-compose -f production.yml up
    


# con entorno
1. Crear entorno virtual:

```bash
python3  -m venv venv
```

2. Activar el entorno virtual:

```bash
python -m venv/bin/activate
```

3. Instalar las dependencias necesarias:

```bash
python -m pip install -r requirements.txt
```


4. Correr la aplicación y levantar servidor para los endpoints:

```bash
cd app
python3 main.py
```