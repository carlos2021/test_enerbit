import json
import os


def get_parser_data(data):
    decode = True
    try:
        json_str = str(str(data).replace("b'", "'"))
        json_data = json.loads(json_str.replace("'", '"'))
        return json_data, decode
    except:
        return data, not (decode)


def get_metric_from_kwh(metrics_str: str) -> int:
    try:
        return int(metrics_str.replace(" kwh", ""))
    except:
        return 0


def is_an_alert_to_report(metrics_str: str) -> bool:
    metrics_int = get_metric_from_kwh(metrics_str)
    return True if metrics_int > 50 else False


def generated_alert(data_report: json):
    f = open("reports/reports.txt", "a")
    message_alert = f"el dispositivo { data_report['device_id'] } ha superado el umbral con una medida de { data_report['metrics'] } a las {data_report['timestamp']}\n"
    f.write(message_alert)
    f.close()
    print(
        "\a"
    )  # intentando hacer sonido sonido pero mi maquina no tiene entonces no se si sirva jajajajjaja
