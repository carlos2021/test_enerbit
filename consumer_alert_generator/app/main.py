from core.config import connect_to_redis, get_data
import asyncio


async def consumer():
    connection = connect_to_redis()
    get_data(connection)


async def main():
    await consumer()


asyncio.run(main())
