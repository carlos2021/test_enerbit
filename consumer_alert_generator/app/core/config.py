from redis import Redis
import json
from utilities.report_errors import (
    generated_alert,
    is_an_alert_to_report,
    get_parser_data,
)

stream_key = "reported-metrics"

import os
from dotenv import load_dotenv

load_dotenv()


def connect_to_redis():
    hostname = os.getenv("HOST_REDIS")
    port = 6379

    r = Redis(hostname, port, retry_on_timeout=True)
    return r


def get_data(redis_connection):
    last_id = 0
    sleep_ms = 5000
    while True:
        try:
            resp = redis_connection.xread(
                {stream_key: last_id}, count=1, block=sleep_ms
            )
            if resp:
                key, messages = resp[0]
                last_id, data = messages[0]
                json_data, is_decode = get_parser_data(data)
                if is_decode:
                    if is_an_alert_to_report(json_data["metrics"]):
                        generated_alert(json_data)


        except ConnectionError as e:
            print("ERROR REDIS CONNECTION: {}".format(e))
