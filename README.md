# Prueba tecnica para EnerBit 

Esta es una prueba tecnica que se compone de cuatro microservicios que se interconectan y utilizan redis, consumers y producers

![diagrama del proyecto](/documentation/diagrama.jpeg)


## Correr el proyecto usando entorno virtuales

lee y ejecuta cada readme de cada servicio, aunque el redis si lo tendras que levantar con docker o darle la dirección de tu propio redis, mi recomendación personal es ponerlo a correr asi por que de la otra forma toca que des el host de docker del container de redis


## Correr el proyecto usando Docker

Podes correr cada microservicio como gustes o seguir estos pasos desde la raiz del proyecto:

1 - levantar redis
```sh
    sudo docker-compose -f redis/redis-docker-compose.yml build
    sudo docker-compose -f redis/redis-docker-compose.yml up
```

2 - levantar el producer
```sh
    sudo docker-compose -f measurement_report_simulator/local.yml build
    sudo docker-compose -f measurement_report_simulator/local.yml up
```

3- levantar el microservicio del crud
```sh
    sudo docker-compose -f crud_metrics/local.yml build
    sudo docker-compose -f crud_metrics/local.yml up
```
4- levantar los dos consumers
```sh
    sudo docker-compose -f consumer_alert_generator/local.yml build
    sudo docker-compose -f consumer_alert_generator/local.yml up
    sudo docker-compose -f consumer_crud_metrics/local.yml build
    sudo docker-compose -f consumer_crud_metrics/local.yml up
```


![diagrama del proyecto](/documentation/run.png)