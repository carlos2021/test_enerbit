# Prueba tecnica para enerbit 

este es un microservicio que realiza el rol de Consumer.


## Detalle

un microservicio con python que escuche los eventos reportados
por el simulador y llame al microservicio de fastapi o django para
almacenar las métricas.
Nota: Este servicio tendrá rol de Consumer, tener en cuenta que cada
evento consumido se deberá quitar de la lista del consumidor.

# Variables de entorno

Solo debes copiar y pegar el archivo .envExample con el nombre .env, en el caso de que quieras correrlo con docker pues dar la direccion de redis para que tome el host del contenedor que levanta el docker, en caso de que quieras correrlo con entorno virtual pues localhost, y pues si vas a conectarte a otro redis y no el que genera este proyecto pues le pones el host

# con docker

## Docker

El proyecto esta fuentemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción. 


## Ponerlo a correr
    $ sudo docker-compose -f local.yml build
    $ sudo docker-compose -f local.yml up




## Despliegue a producción
    $ sudo docker-compose -f production.yml build
    $ sudo docker-compose -f production.yml up
    


# con entorno
1. Crear entorno virtual:

```bash
python3  -m venv venv
```

2. Activar el entorno virtual:

```bash
python -m venv/bin/activate
```

3. Instalar las dependencias necesarias:

```bash
python -m pip install -r requirements.txt
```


4. Correr la aplicación y levantar servidor para los endpoints:

```bash
cd app
python3 main.py
```