import requests
import os
from dotenv import load_dotenv

load_dotenv()


URL_MICROSERVICE_CRUD = os.getenv("HOST_MICROSERVICE_CRUD")


class Conector_microservice_crud:
    def create(self, data):
        url = URL_MICROSERVICE_CRUD + "report/"
        myobj = {
            "pk": data["uuid"],
            "id_device": data["device_id"],
            "metrics": data["metrics"],
            "timestamp": data["timestamp"],
        }
        response = requests.post(url, json=myobj)
        return response.json()

    def read(self):
        url = URL_MICROSERVICE_CRUD + "reports/"
        response = requests.get(url)
        return response.json()

    def update(self, id, data):
        url = f"{URL_MICROSERVICE_CRUD}reports/{id}"
        response = requests.put(url, data)
        return response.json()

    def delete(self, id):
        url = f"{URL_MICROSERVICE_CRUD}report/{id}"
        response = requests.delete(url)
        return response.json()
