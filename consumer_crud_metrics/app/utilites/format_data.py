import json
import os


def get_parser_data(data):
    decode = True
    try:
        json_str = str(str(data).replace("b'", "'"))
        json_data = json.loads(json_str.replace("'", '"'))
        return json_data, decode
    except:
        return data, not (decode)
