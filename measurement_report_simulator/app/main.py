from core.config import connect_to_redis, send_data
import asyncio


async def simulator():
    connection = connect_to_redis()
    send_data(connection)


async def main():
    await simulator()


asyncio.run(main())
