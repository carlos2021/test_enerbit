import random

DEVICE_REFERENCES = {
    "Teléfono": "31dasda12147",
    "Tablet": "fsdaf12sa141",
    "Portátil": "dsfasda121e4",
    "Cámara": "gsf131r13j16",
    "aspirador": "fds141445535",
    "robot": "pfdfdas14662",
    "Enchufes": "dfsf14414312",
    "Localizador": "ldasoqwq2314",
    "Televisor": "pfly125799qw",
}


def generate_random_id_device() -> str:
    return DEVICE_REFERENCES[random.choice(list(DEVICE_REFERENCES))]
