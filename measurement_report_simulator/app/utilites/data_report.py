import random
import datetime
from uuid import uuid4
from utilites.generator_random_device import generate_random_id_device


def simulator_data():
    return {
        "uuid": uuid4().hex,
        "device_id": generate_random_id_device(),
        "metrics": str(random.randint(10, 100)) + " kwh",
        "timestamp": str(datetime.datetime.now()),
    }
