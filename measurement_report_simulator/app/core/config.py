from redis import Redis
from time import sleep
from utilites.data_report import simulator_data
import os
from dotenv import load_dotenv

load_dotenv()


stream_key = "reported-metrics"
producer = "electrical-device"


def connect_to_redis():
    hostname = os.getenv("DB_HOST_REDIS")
    port = 6379

    r = Redis(hostname, port, retry_on_timeout=True)
    return r


def send_data(redis_connection):
    while True:
        try:
            data = simulator_data()
            resp = redis_connection.xadd(stream_key, data)
            print(resp)

        except ConnectionError as e:
            print("ERROR REDIS CONNECTION: {}".format(e))
        sleep(0.5)
